# Coursework

* name: your-name-here
* email: your-uw-email-here

## Instructions

1. Fork this repository, and name it in the following format:
  - `yourname_coursename`   e.g.  `steve_intro_php`

2. add your name and email above, then share your fork, READ ONLY, with me: edu@pagerange.com

3. When you are working on labs and assignments, make sure to create a branch for your work, add and commit your work regularly, and push to BitBucket when you are finished.

4. Always finish by merging your work into master branch, with a message like ASSIGNMENT 1 COMPLETE or LAB 3 COMPLETE, etc.

5. You don't have to email me to let me know your work is complete, I can see it in your commits and pushes.  It is **your** responsibility to make sure your branches are properly merged, and that you have pushed your work to BitBucket.

_@updated 13 February 2021_

---
  
  