/**
 * Javascript for Lab 5
 * This is the only file you should edit!
 */

// on page load
$(document).ready(function(){
	
	// click to disable all deprecated links
	$('#remove_deprecated').click(function(e){
		e.preventDefault();
		
		// change href attribute value to javascript void 0, and change the text decoration underline and line-through
		$('ul.nav_list li.deprecated a').attr('href', "javascript: void(0);").css({"cursor": "pointer", "text-decoration": "underline line-through"});
	});

	// click to make h2 color red which is inside .main div
	$('#main_h2').click(function(e){
		e.preventDefault();
		$('div.main > h2').css({"color": "red"});
	});
	
	// click to make h3 color blue which is inside .secondary div
	$('#secondary_h3').click(function(e){
		e.preventDefault();
		$('div.secondary > h3').css({"color": "blue"});
	});
	
	// click to make first paragraph element font bold which is inside .main div
	$('#bold_p_main').click(function(e){
		e.preventDefault();
		$('div.main > p:first-of-type').css({"font-weight": "bold"});
	});
	
	// click to make last paragraph element font bold which is inside .secondary div
	$('#bold_p_secondary').click(function(e){
		e.preventDefault();
		$('div.secondary > p:last-of-type').css({"font-weight": "bold"});
	});
	
	// click to hide all announcement
	$('#hide_annoying').click(function(e){
		e.preventDefault();
		$('div.annoying').hide('slow');
	});
	
	// click to show all announcement
	$('#show_annoying').click(function(e){
		e.preventDefault();
		$('div.annoying').show('slow');
	});
	
	////////////////////////// Now i feel conmfortable, thanks to WDD ////////////
	
	// click to animate the black box to right upto 800px
	$('#move_right').click(function(e){
		e.preventDefault();
		
		// move box to right with slow animation effect in 1.7 seconds
		$( "#move_me" ).animate({
			left: "800",
			}, 2000, function() {
  	});
	});
	
	// click to animate the black box to back to its actual position
	$('#move_left').click(function(e){
		e.preventDefault();
		
		// move box to left with slow animation effect in 1.7 seconds
		$( "#move_me" ).animate({
			left: "0",
			}, 2000, function() {
  	});
	});
	
});


