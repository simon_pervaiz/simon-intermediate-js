/**
 * All your Javacript goes here.
 * Do not modify index.html in any way
 */

// function to perform onload action
window.onload =  function(e) {
	
	// check the default color set
	checkdefaultcolor();
	
	// change headings color
  changecolor();
	
}

// function to change the color of headings to last saved color retreived cookie
function checkdefaultcolor() { 
	
	var colorcookie = document.cookie.split(',');
  defaultcolor = colorcookie[0].split('=');
	
	// select all headings in a document
	var headings = document.querySelectorAll("h1,h2,h3,h4,h5,h6");
	for(i=0; i<headings.length; i++) {
		headings[i].style.color = defaultcolor[1];
	}
	
}

// function to change the headings color by click the color pallet
function changecolor() {

	// select all color pallets
	var color = document.querySelectorAll("td");

	// select all headings in a document
	var headings = document.querySelectorAll("h1,h2,h3,h4,h5,h6");

	// set expiry for cookie 1 year
	var expiry = 60*60*24*365;

	if(color) {
		for(i=0; i<color.length; i++) {
			color[i].onclick = function(e) {
				for(i=0; i<headings.length; i++) {
					headings[i].style.color = this.dataset.clr;
				}
				document.cookie = "color=" + this.dataset.clr + "; max-age="+expiry;
			}
		}
	}
	
}